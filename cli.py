import argparse


def parse_arguments():
    parser = argparse.ArgumentParser()

    # SSH related stuff
    parser.add_argument(
        "--ssh-host",
        help="The ssh host to connect to",
        required=False
    )

    parser.add_argument(
        "--ssh-user",
        help="The name of the user in the remote host. (Always required if --ssh is set)",
        required=False
    )

    parser.add_argument(
        "--ssh-user-pass",
        help="The password of the user in the remote host. (Only required if --ssh-pk is not set)",
        required=False
    )

    parser.add_argument(
        "--ssh-port",
        help="The port of the server to connect with ssh to",
        required=False
    )

    parser.add_argument(
        "--injections",
        help="The list of scripts that will be run on the remote machine.",
        nargs='+',
        required=False
    )

    # Input file:
    parser.add_argument(
        "--input-file",
        help="Specify an input file to be injected. Scraping is ignored altogether",
        required=False
    )

    parser.add_argument(
        "--output-file",
        help="Specify the output file.",
        required=False
    )

    # Ionos authentication
    parser.add_argument(
        "--ionos-user",
        help="The ionos username to authenticate as",
        required=False
    )

    parser.add_argument(
        "--ionos-pass",
        help="The ionos password to use",
        required=False
    )

    # Chrome-Driver
    parser.add_argument(
        "--user-dir",
        default="./user_dir",
        required=False
    )

    parser.add_argument(
        "--no-headless",
        default=False,
        required=False
    )

    parser.add_argument(
        "--create-dbs",
        default=False,
        required=False
    )

    parser.add_argument(
        "--old-db-version",
        default="MySQL 5.5",
        required=False
    )

    parser.add_argument(
        "--new-db-version",
        default="MySQL 5.7",
        required=False
    )

    return parser.parse_args(), parser
