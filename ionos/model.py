from dataclasses import dataclass


@dataclass
class IonosOptions:
    cookies_file: str = None
    refresh_token: bool = True
    user_dir: str = None
    headless: bool = None


@dataclass
class IonosDatabase:
    database: str
    description: str
    hostname: str
    port: str
    username: str
    type: str


@dataclass
class IonosCredentials:
    username: str
    password: str


@dataclass
class DatabaseMigrationParams:
    donor: IonosDatabase
    receiver: IonosDatabase
    password: str
