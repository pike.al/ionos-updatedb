from ionos.exceptions import IonosAuthException
from ionos.urls import URLS
from .utils import get_browser_html_tree


def login(client):
    """Given the IonosClient, do the login."""
    # Open the login url
    client.browser.get(URLS.LOGIN)

    print("Please accept cookies and press enter...")
    input()

    # Fill the form
    client.browser \
        .find_element_by_id("login-form-user") \
        .send_keys(client.credentials.username)

    client.browser \
        .find_element_by_id("login-form-password") \
        .send_keys(client.credentials.password)

    client.browser \
        .find_element_by_xpath(".//*[contains(text(), 'Eingeloggt bleiben')]") \
        .click()

    input("Filled fields. Press enter to confirm...")

    # Submit the form
    client.browser.find_element_by_id("login-form-submit").click()

    page = get_browser_html_tree(client)

    is_password_invalid = len(page.xpath('.//p[contains(text(),"Passwort ist nicht korrekt")]')) > 0

    is_zip_code_required = len(page.xpath('.//span[contains(text(),"Postleitzahl")]')) > 0

    if is_zip_code_required:
        raise IonosAuthException("Zip code input is required to log in again.")

    if is_password_invalid:
        raise IonosAuthException("Password is incorrect. Please try again!")

    # If we are still unauthorized and reason is not known
    if client.is_unauthorized(ignore_state=True):
        raise IonosAuthException(
            "Authentication failed. Please try to login via browser to solve potential security challenges."
        )


def is_unauthorized(client):
    url = client.browser.current_url
    if url.startswith(URLS.LOGIN):
        return True

    return False
