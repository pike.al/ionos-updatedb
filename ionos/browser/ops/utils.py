from lxml import html


def get_html_tree(raw):
    return html.fromstring(raw)


def get_browser_html(client):
    return client.browser.page_source


def get_browser_html_tree(client):
    return get_html_tree(get_browser_html(client))
