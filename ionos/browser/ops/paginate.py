class IonosPaginate:
    def __init__(self, client, page_url, per_page):
        """Initialize the IonosPaginate object

        :param client: The client object
        :param page_url: The URL to the partial pagination controller component
            , that will be parametrized by two integers, the page_count and page.
        """

        self.client = client
        self.page_url = page_url

        self.per_page = per_page
        self.page = 1

        # Get the page count
        self.page_count = self.get_page_count()

    def __iter__(self):
        # It is its own iterator
        return self

    def __next__(self):
        if self.page > self.page_count:
            raise StopIteration
            # raise IndexError(f"Attempting to open page with index {self.page+1}, out of {self.page_count} pages")

        result = self.get_current_page()
        self.page += 1

        return result

    def get_current_page(self):
        filled_url = self.page_url % (self.per_page, self.page)

        self.client.browser.get(filled_url)

        return self.client.get_html()

    def get_page_count(self):
        filled_url = self.page_url % (self.per_page, self.page)
        print(filled_url)
        self.client.browser.get(filled_url)

        tree = self.client.get_html_tree()

        page_count = tree.cssselect("ul.content-pagination")[0].attrib['data-total-pages']

        return int(page_count)
