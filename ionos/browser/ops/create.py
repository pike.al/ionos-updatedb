from ...urls import URLS
from ...utils import halt


def create_database(driver, version, description, password):
    driver.get(URLS.BASE_URL + "/mysql-create-database")

    # Select new MySQL version
    driver \
        .find_element_by_xpath(f"//select[@name='mysql-new-database.version']/option[text()='{version}']") \
        .click()

    # Fill database description
    desc_element = driver.find_element_by_name("mysql-new-database.description")
    desc_element.send_keys(description)

    # Fill database password
    pass_element = driver.find_element_by_name("mysql-new-database.password")
    pass_element.send_keys(password)

    halt(2)

    # Submit form
    driver.find_element_by_id("createMysqlDBForm").submit()

    return driver.page_source
