from ionos.urls import URLS
from .paginate import IonosPaginate
from ...model import IonosDatabase


def retrieve_db_info(page):
    # Map of database details
    attrs = {"Datenbank": page.cssselect(".headline-page")[0].text_content().split()[1]}

    # Iterate detail table rows
    for elem in page.cssselect(".stripe-item"):
        # Get columns
        children = elem.getchildren()

        # Discard items that don't have the minimum number of columns
        if len(children) < 2:
            continue

        # The first column is the key
        key = children[0].text_content()

        # The second column has the value
        value = children[1].text_content()

        # Save key-value mapping
        attrs[key] = value

    obj = IonosDatabase(
        description=attrs["Beschreibung"],
        hostname=attrs["Hostname"],
        port=attrs["Port"],
        database=attrs["Datenbank"],
        username=attrs["Benutzername"],
        type=attrs["Typ und Version"]
    )

    # Return database object
    return obj


class MySqlPaginate(IonosPaginate):
    def __init__(self, client, per_page=20):
        # Preparation
        client.browser.get(URLS.HOME)
        client.browser \
            .find_element_by_xpath("//a[starts-with(@href, '/hosting-portfolio')]") \
            .click()

        # Call parent's constructor
        super().__init__(client, URLS.MYSQL_PAGE, per_page)
