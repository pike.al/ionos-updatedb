import pickle

from selenium.webdriver.chrome.options import Options
from seleniumrequests import Chrome

from ionos.urls import URLS


# def set_up_browser(cookies_file):
#      """
#      Initializes a Mechanize browser with a user-agent and a cookiejar.

#      :param cookies_file: Path to the file that will be used to store cookies.
#      :return: Configured Mechanize browser instance.
#      """

#      # Create browser instance
#      browser = mechanize.Browser()

#      # robots.txt is not important for our purposes
#      browser.set_handle_robots(False)

#      # Add unsuspicious user-agent
#      browser.addheaders = [(
#          'user-agent',
#          'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36'
#      )]

#      # Create cookie jar and attach it to browser
#      cookiejar = LWPCookieJar()
#      browser.set_cookiejar(cookiejar)

#      # Check if filename parameter is valid
#      if cookies_file is None:
#          raise ValueError("Invalid cookie filename.")

#      if not path.exists(cookies_file):
#          # Cookies file doesn't exist yet
#          print(f"Creating cookies file: {cookies_file}.")
#          browser.cookiejar.save(cookies_file)
#      else:
#          print(f"Loading cookies from file {cookies_file}.")
#          browser.cookiejar.load(cookies_file, ignore_discard=True)

#      return browser


def set_up_browser(user_dir=None, headless=False, get_wrapper=None):
    options = Options()

    if headless:
        options.add_argument("--headless")

    if user_dir is not None:
        print(user_dir)
        options.add_argument(f"--user-data-dir={user_dir}")

    browser = Chrome(options=options)

    if get_wrapper is not None:
        browser.get = get_wrapper(browser.get)

    # if cookie_file is not None:
    #     if not path.exists(cookie_file):
    #         # Throw some exception?
    #         # Or do nothing
    #         pass
    #     else:
    #         load_cookies(browser, cookie_file)

    return browser


def load_cookies(browser, cookie_file):
    """Loads cookies if there's such a file"""
    browser.get(URLS.HOME)

    print("Loading cookies")

    with open(cookie_file, "rb") as file:
        cookies = pickle.load(file)
        for cookie in cookies:
            if cookie['domain'] == '.ionos.de':
                browser.add_cookie(cookie)


def dump_cookies(browser, cookie_file):
    with open(cookie_file, "wb") as file:
        pickle.dump(browser.get_cookies(), file)
