import dataclasses
import json
import time


def halt(span=5, forever=False):
    if forever:
        input("Waiting for input")
        return

    print(f"Sleeping for {span} seconds")
    time.sleep(span)


class EnhancedJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if dataclasses.is_dataclass(o):
            return dataclasses.asdict(o)
        return super().default(o)
