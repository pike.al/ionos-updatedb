class URLS:
    BASE_URL = "https://mein.ionos.de"
    HOME = "https://mein.ionos.de/product-overview"
    LOGIN = "https://login.ionos.de"
    MYSQL_PAGE = "https://mein.ionos.de/mysql-overview?page.size=%d&page.page=%d"
