class IonosAuthException(Exception):
    def __init__(self, message="Not logged in!"):
        self.message = message
        super().__init__(self.message)
