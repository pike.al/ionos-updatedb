from enum import Enum

from . import browser as b
from .browser import ops as bops
from .browser.ops import mysql
from .browser.ops.mysql import MySqlPaginate
from .browser.ops.utils import get_browser_html, get_browser_html_tree, get_html_tree
from .urls import URLS


def get_wrapper(get):
    def wrapper(*args, **kwargs):
        print(f"Openging {args[0]}")
        input("Hello")
        return get(*args, **kwargs)

    return wrapper(get)


class IonosClient:
    def __init__(self, credentials, options):
        self.state = AuthState.UNAUTHORIZED

        self.refresh_token = options.refresh_token

        self.credentials = credentials
        self.options = options

        self.browser = b.set_up_browser(user_dir=self.options.user_dir,
                                        headless=options.headless)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        print(self.browser)
        if self.browser is not None:
            self.browser.quit()

    def login(self):
        # Only log in if not already logged in
        self.browser.get(URLS.HOME)

        if self.is_unauthorized(ignore_state=True):
            print("Unauthorized")
            bops.auth.login(self)
        else:
            print("Cookies were valid!")

        print("Authorized")

        self.state = AuthState.SOMESTATE

    def create_database(self, version, description, password):
        page = bops.create.create_database(self.browser, version, description, password)

        while True:
            try:
                return mysql.retrieve_db_info(get_html_tree(page))
            except Exception as e:
                print(e)
                print(f"Please manually go to details page for {description} and press Enter to continue...")
                input()
                page = self.get_html()

    def get_mysql_links(self):
        pages = self.get_mysql_pages()

        links = []

        for page in pages:
            root = get_html_tree(page)
            for elem in root.cssselect("a.link-standard"):
                links.append(elem.get('href'))

        return links

    def get_deprecated_mysql_dbs(self, old_version):
        links = self.get_mysql_links()

        all_dbs = [self.retrieve_db_info(link) for link in links]

        return [db for db in all_dbs if db.type == old_version]

    def retrieve_db_info(self, path):
        """
        Creates a Database object by scraping the info on the given Ionos database details page.

        :param path: Path to the Ionos database details page.
        :return: Database object.
        """

        self.browser.get(URLS.BASE_URL + path)

        return bops.mysql.retrieve_db_info(self.get_html_tree())

    def get_html(self):
        return get_browser_html(self)

    def get_html_tree(self):
        return get_browser_html_tree(self)

    def is_unauthorized(self, ignore_state=False):
        """Given the IonosClient, check if we are currently unauthorized.
        Obvious case is that in which the current state is set to UNAUTHORIZED.

        Otherwise, we scrape the page, or check the url."""

        if (not ignore_state) and self.state == AuthState.UNAUTHORIZED:
            return True

        return bops.auth.is_unauthorized(self)

    def get_mysql_pages(self):
        """ Return the page iterator, given the mysql_pages URL
        and the mysql_page URL"""

        return MySqlPaginate(self)

    def save_cookies(self):
        if self.options.cookies_file:
            b.dump_cookies(self.browser, self.options.cookies_file)


class AuthState(Enum):
    UNAUTHORIZED = 0
    SOMESTATE = 100
