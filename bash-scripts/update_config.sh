#!/bin/bash

JQ="$HOME/jq"
# HTDOCS='/kunden/homepages/23/d564018964/htdocs/websites/'
HTDOCS='/kunden/homepages/23/d564018964/htdocs/websites/testsite'

# Get JSON object from filename or stdin
JSON="$(cat "${1:-/dev/stdin}" | $JQ '.')"

JSON=$(echo "$JSON" | "$JQ" '[.[] | {(.donor.database) : [.receiver, {password: .password}] | add}] | add')

WP_CONFIG_LIST="$(find $HTDOCS -type f -name 'wp-config.php')"

echo "$WP_CONFIG_LIST" | while read FILENAME; do
    [[ "$FAIL" -eq "1" ]] && continue

	cp "$FILENAME" "$FILENAME.bak"
    echo "Took backup of wp config file: $FILENAME"

	DONOR_DB=$(grep 'DB_NAME' $FILENAME \
		| sed "s/.*['\"]\(.*\)['\"].*);.*/\1/g")

	DB_OBJECT=$(echo "$JSON" | "$JQ" ".$DONOR_DB")

    [[ "$DB_OBJECT" == "null" ]] \
        && echo 'DB not found in input' \
        && continue

	DB_NAME=$(echo "$DB_OBJECT" | "$JQ" -r '.database')
	DB_USER=$(echo "$DB_OBJECT" | "$JQ" -r '.username')
	DB_PASS=$(echo "$DB_OBJECT" | "$JQ" -r '.password')
	DB_HOST=$(echo "$DB_OBJECT" | "$JQ" -r '.hostname')

	sed -i "s/.*define(.*['\"]DB_PASSWORD['\"].*/define( 'DB_PASSWORD', '$DB_PASS' )\;/g" "$FILENAME" || FAIL=1
	sed -i "s/.*define(.*['\"]DB_NAME['\"].*/define( 'DB_NAME', '$DB_NAME' )\;/g"     "$FILENAME" || FAIL=1
	sed -i "s/.*define(.*['\"]DB_USER['\"].*/define( 'DB_USER', '$DB_USER' )\;/g"     "$FILENAME" || FAIL=1
	sed -i "s/.*define(.*['\"]DB_HOST['\"].*/define( 'DB_HOST', '$DB_HOST' )\;/g"     "$FILENAME" || FAIL=1
    FAIL=1

    [[ "$FAIL" -eq "1" ]] && continue

    echo "Updated config for: $FILENAME"
    echo
    echo
done
