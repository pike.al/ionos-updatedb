#!/bin/bash -x

which jq
JQ_INSTALLED="$?"

JQ="$HOME/jq"

[[ "$JQ_INSTALLED" -eq "0" ]] \
    || curl -s -L https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64 > "$JQ" \
    && chmod +x "$JQ"

[[ "$JQ_INSTALLED" -eq 0 ]] && JQ="$(which jq)"

# Get JSON object from filename or stdin
JSON="$(cat "${1:-/dev/stdin}" | $JQ '.')"

# Get number of databases in the 'databases' object
DB_COUNT="$(echo "$JSON" | $JQ length)"

# Decrement to get max index
let DB_COUNT--

for INDEX in $(seq 0 "$DB_COUNT"); do
  [[ "$FAILED" -eq "1" ]] && continue
  OBJECT="$(echo "$JSON" | $JQ -r --arg i "$INDEX" '.[$i | tonumber]')"

  DB_PASS="$(echo "$OBJECT" | $JQ -r '.password')"

  DONOR_DB_USER="$(echo "$OBJECT" | $JQ -r '.donor.username')"
  DONOR_DB_NAME="$(echo "$OBJECT" | $JQ -r '.donor.database')"
  DONOR_DB_HOST="$(echo "$OBJECT" | $JQ -r '.donor.hostname')"

  echo "Donor database details:"
  echo "$DONOR_DB_USER"
  echo "$DONOR_DB_NAME"
  echo "$DONOR_DB_HOST"

  RECEIVER_DB_USER="$(echo "$OBJECT" | $JQ -r '.receiver.username')"
  RECEIVER_DB_NAME="$(echo "$OBJECT" | $JQ -r '.receiver.database')"
  RECEIVER_DB_HOST="$(echo "$OBJECT" | $JQ -r '.receiver.hostname')"

  echo "Receiver database details:"
  echo "$RECEIVER_DB_USER"
  echo "$RECEIVER_DB_NAME"
  echo "$RECEIVER_DB_HOST"

  FILENAME="$DB_NAME.sql"

  MYSQL_PWD="$DB_PASS" mysqldump -h "$DONOR_DB_HOST" "-u'$DONOR_DB_USER'" "$DONOR_DB_NAME" | tee "$FILENAME" >/dev/null
  [[ "${PIPESTATUS[0]}" -eq "0" ]] || (echo 'The dump command failed' && FAILED=1)
  [[ "$FAILED" -eq "1" ]] && continue


  echo "Importing in new database..."

  mysql -h "$RECEIVER_DB_HOST" -u "$RECEIVER_DB_USER" "-p$DB_PASS" "$RECEIVER_DB_NAME" < "$FILENAME"

  echo "All done!"
done
