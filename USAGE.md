# IONOS Database Migrator

## IONOS commands

All the commands below have the following base arguments.

The Chrome user directory path that is used is defined by the `user-dir` argument. 
If omitted, it defaults to `./user_dir`.

If omitted, `no-headless` defaults to `true`. 
This is the recommended mode to use as there are certain situations where user input may be required.

    python3 main.py 
        --ionos-user <user> 
        --ionos-pass <pass>
        [ --no-headless <true/false> ]
        [ --user-dir <directory>]

These common arguments are not included below for brevity.

### Getting a list of deprecated databases
This will log in to Ionos, scrape all databases running an old version, and then outputs them as a JSON array.

The `old-db-version` argument defaults to `MySQL 5.5` and follows IONOS' naming.

If an output filename is not specified, then the result is printed to stdout.

    python3 main.py 
        ...
        [ --old-db-version <version> ]
        [ --output-file dbs.json ]

### Creating new databases and generating migration JSON
This command does everything as the one above, creates new Ionos databases for each deprecated one, 
and then outputs a JSON that can be used for the migration.

The `new-db-version` argument defaults to `MySQL 5.7` and follows IONOS' naming.

If an `input-file` is specified, the scraping part is skipped and the list of databases is retrieved from the file.

    python3 main.py 
        ...
        --create-dbs true
        [ --old-db-version <version> ]
        [ --new-db-version <version> ]
        [ --output-file migrations.json ]
        [ --input-file dbs.json ]

## Migration commands
All the commands below have the following base arguments.
The `ionos-pass` argument is used as the databases' password so it is still required.
The `input-file` is the migration JSON file.

    python3 main.py 
        --input-file <path>
        --ionos-pass <pass>

### Migrating databases
This command needs a migration JSON as an input file.

    python3 main.py 
        ...
        --injections ./bash-scripts/copy_database.sh

### Deleting deprecated databases
This command uses the same input as above.

    python3 main.py 
        ...
        --injections ./bash-scripts/delete_database.sh

### Running custom scripts
You can also run other scripts as needed. 

Simply put the script's path in the `injections` argument and pass what you need to `input-file`.
