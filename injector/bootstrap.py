import os
import shutil

from subprocess import Popen, PIPE

# This should be made more reasonable
SCRIPT_DIR = '/tmp/injection_dir'


# Actual bootstrapping:
def check_environment():
    """Check whether the environment is supported."""
    return True


def bootstrap_directory():
    if os.path.exists(SCRIPT_DIR) and not os.path.isdir(SCRIPT_DIR):
        os.rmdir(SCRIPT_DIR)

    if not os.path.exists(SCRIPT_DIR):
        os.mkdir(SCRIPT_DIR)


def bootstrap():
    try:
        check_environment()
    except Exception as e:
        return str(e)

    bootstrap_directory()


# Cleanup:
def cleanup_directory():
    if os.path.exists(SCRIPT_DIR):
        shutil.rmtree(SCRIPT_DIR)


def cleanup():
    cleanup_directory()


# Write the shell script
def write_shell_script(script_name, blob):
    """Given a blob file, write it to RAM."""

    script_path = os.path.join(SCRIPT_DIR, script_name)
    with open(script_path, 'wb') as script_file:
        script_file.write(blob)

    # Not that important:
    # os.chmod(script_path, '+x')

    return True


# Run the shell script with the input sent by master
def run_shell_script(script_name, input_blob):
    script_path = os.path.join(SCRIPT_DIR, script_name)
    p = Popen(['bash', '-c', script_path], stdout=PIPE, stdin=PIPE, stderr=PIPE)
    stdoutputs = p.communicate(input=input_blob)

    return stdoutputs


def copy(
        donor_db_hostname, donor_db_username, donor_db_description,
        recv_db_hostname, recv_db_username, recv_db_description,
        password
):
    recv_comm = f'mysql -h "{recv_db_hostname}" -u "{recv_db_username}" -p"{password}" "{recv_db_description}"'

    recv = Popen(
        recv_comm.split(),
        stdin=PIPE,
        stdout=PIPE
    )

    donor_comm = f'mysqldump -h "{donor_db_hostname}" -u "{donor_db_username}" -p"{password}" "{donor_db_description}"'

    donor = Popen(
        donor_comm.split(),
        stdout=recv.stdin
    )

    recv_stdout = recv.communicate()[0]
    donor.wait()

    return recv_stdout
