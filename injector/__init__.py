import contextlib
import os

import mitogen.core
import mitogen.master

from . import bootstrap


class ScriptInjector:
    def __init__(self, hostname=None, password=None, username=None, port=None):
        self.hostname = hostname
        self.password = password
        self.username = username
        self.port = port
        self.sshlave = None

        # Bootstrap mitogen:
        self._mitogen_setup()

    def _mitogen_setup(self):
        self.broker = mitogen.master.Broker()
        self.router = mitogen.master.Router()

    def do_ssh_login(self):
        """Try to login. This might throw some exception."""
        # *chuckle*
        self.sshlave = self.router.ssh(
            hostname=self.hostname,
            password=self.password,
            username=self.username,
            port=self.port,
            python_path="/usr/bin/python3"
        )

        # After logging in, do the necessary bootstrapping:
        self.sshlave.call(bootstrap.bootstrap)

    # Context management, for cleaning up mitogen stuff
    def __enter__(self):
        self._exit_stack = contextlib.ExitStack()
        self._exit_stack.enter_context(self.router)
        return self

    def __exit__(self, type, value, traceback):
        # Call cleanup
        self.sshlave.call(bootstrap.cleanup)

        # Close the ExitStack
        self._exit_stack.close()

    def inject_script(self, location, remote_filename=None):
        filename = remote_filename or os.path.basename(location)

        with open(location, 'rb') as file:
            return self.sshlave.call(bootstrap.write_shell_script, filename, file.read())

    def run_script(self, location, stdinput, remote_filename=None):
        filename = remote_filename or os.path.basename(location)

        return self.sshlave.call(bootstrap.run_shell_script, filename, stdinput)
    
    def copy(self, hostname1, username1, description1, hostname2, username2, description2, ionos_pass):
        self.sshlave.call(bootstrap.copy, hostname1, username1, description2, hostname2, username2, description2, ionos_pass)
