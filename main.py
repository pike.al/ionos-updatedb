# Used to parse and handle cli arguments
import getpass
import json
import sys

from cli import parse_arguments
from injector import ScriptInjector
from ionos import IonosClient
from ionos.model import IonosCredentials, IonosOptions, DatabaseMigrationParams
from ionos.utils import EnhancedJSONEncoder, halt


def main():
    args, parser = parse_arguments()
    json_output_obj = None

    if args.input_file is None:
        credentials = IonosCredentials(args.ionos_user, args.ionos_pass)
        options = IonosOptions(user_dir=args.user_dir, refresh_token=False, headless=not args.no_headless)

        with IonosClient(credentials, options) as client:
            # Check validity of ionos arguments
            if args.ionos_user is None:
                parser.print_help()
                sys.exit("Please specify the ionos user")

            if args.ionos_pass is None:
                args.ionos_pass = getpass.getpass(prompt="Ionos User Password: ")

            # Do the logging in (will actually use the cookie, if it is valid)
            try:
                client.login()
                print("Login successful!")
            except Exception as e:
                print("There was some error!")
                print(e)
                return

            print("Saving cookie for later use")
            client.save_cookies()

            print("Press Enter...")
            input()

            print("Retrieving dbs")
            dbs = client.get_deprecated_mysql_dbs(old_version=args.old_db_version)
            json_output_obj = dbs

            param_list = []

            if args.create_dbs:
                for db in dbs:
                    version = args.new_db_version
                    new_description = "update_" + db.description
                    password = args.ionos_pass

                    print(f"Creating new database {new_description}...")
                    new_db = client.create_database(version, new_description, password)
                    print(f"Created {new_db}")

                    params = DatabaseMigrationParams(donor=db, receiver=new_db, password=password)

                    param_list.append(params)

                    halt(2)

                json_output_obj = param_list

            dumps = json.dumps(json_output_obj, cls=EnhancedJSONEncoder)

            outfile = args.output_file \
                if args.output_file is not None else sys.stdout

            print(dumps, file=outfile)

            if outfile != sys.stdout:
                outfile.close()

            print("Saved to json!")
    else:
        # No need to login

        dbs_json = None
        with open(args.input_file, "rb") as f:
            dbs_json = f.read()

        print(dbs_json)

        with ScriptInjector(hostname=args.ssh_host, password=args.ssh_user_pass, username=args.ssh_user, port=args.ssh_port) as host:
            # Do the logging in
            host.do_ssh_login()

            if args.injections is not None:
                for injection in args.injections:
                    host.inject_script(injection)

            if args.injections is not None:
                for injection in args.injections:
                    print(f"Injecting {injection}")
                    print("Output is: ")
                    print(host.run_script(injection, dbs_json))


if __name__ == "__main__":
    main()
